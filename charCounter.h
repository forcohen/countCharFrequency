/* Algoritimo desenvolvido na aula de LP1 (linguagem de programação)
/  Autor: Arthur Cohen
/  Curso: Bacharel em Tenologia da Informacao - UFRN
/  04/03/2017
*/

#ifndef CHAR_COUNTER_H
#define CHAR_COUNTER_H

// contabiliza frequencia de caracteres em uma string
void counting(int iPhrase[], int count[], int size);

#endif
