#  Algoritimo desenvolvido na aula de LP1 (linguagem de programação)
#  Autor: Arthur Cohen
#  Curso: Bacharel em Tenologia da Informacao - UFRN
#  04/03/2017

CC=g++
CFLAGS=-Wall -pedantic
EXEC=exec

all: main clean

main: main.o charCounter.o
	$(CC) -o $(EXEC) main.o charCounter.o

charCounter.o: charCounter.cpp charCounter.h
	$(CC) -c charCounter.cpp charCounter.h $(CFLAGS)

main.o: main.cpp
	$(CC) -c main.cpp $(CFLAGS)

clean:
	rm -f *.o *.gch
